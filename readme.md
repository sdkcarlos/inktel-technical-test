# PHP Speech To Text API
#### Video Demo: https://youtu.be/nUHf1_ugBCo
#### Description:

This project consists of a simple implementation of a public REST API that allows users to upload audio files and obtain the transcription, the sentiment analysis of the audio file and returns the number of times those words were spoken, including the timestamp when the word was used.

## Notes

- The API is built on top of [Symfony 6](https://symfony.com/) and [API Platform](https://api-platform.com/docs).
- Allow users to upload an audio file to the server (only MP3 and WAV allowed).
- Allow users to obtain the transcription of the audio file and the sentiment analysis of the audio file using the Google Cloud Speech API (API is available directly, but the official PHP library to access the Speech-To-Text API is available).
- Includes a basic frontend view that allows users to upload an audio file and obtain the transcription and sentiment analysis of the audio file directly in the browser using the built-in API.
- This API is public, so anyone can use it. Security like API keys or other kind of authentication could be implemented upon request.

## Requirements

- PHP 8.1
- MySQL >= 5.7 or Postgresql 13
- gRPC extension for PHP
- Google Cloud Service Account Key File (JSON)

## 1. Create database and build tables

```
php bin/console doctrine:database:create

php bin/console doctrine:schema:update --force
```

## 2. Install and build Swagger assets

```
npm install && npm run build
```

## 3. Start PHP Built-in Server (optional)

The quickest way to set up the project is to use the 

```
symfony server:start --no-tls
```

The project will be accessible without SSL certificate at the following URL: [http://127.0.0.1:8000/](http://127.0.0.1:8000/). Alternatively if you want to use HTTPS, you can use the following commands instead:

```
symfony server:ca:install
symfony server:start
```

The project will be accessible at the following URL: [https://127.0.0.1:8000/](https://127.0.0.1:8000/). 

Alternatively for production environment, a proper web server is required to serve the project (like Caddy, Apache or Nginx).

## 4. Consuming the API

This API has a RESTful compliant design, so it can be consumed by any application. In this case, the examples will be written using cURL. Alternatively you can test them graphically using the [Swagger UI in the API website here(https://test.ourcodeworld.com/apis).

### Uploading a new recordings

```bash
curl -X 'POST' \
  'https://test.ourcodeworld.com/api/processes' \
  -H 'accept: application/ld+json' \
  -H 'Content-Type: multipart/form-data' \
  -F 'file=@carlos-recording.mp3;type=audio/mpeg' \
  -F 'words=[
  "word1",
  "word2"
]' \
  -F 'languageCode=en-US'
```

### Getting a recording's transcription by id

```bash
curl -X 'GET' \
  'https://test.ourcodeworld.com/api/processes/1' \
  -H 'accept: application/json'
```

This request will output a JSON string with the following structure:

```json
{
  "audiofile": "https://test.ourcodeworld.com/uploads/carlos-recording-62814e3c59b11.mp3",
  "words": [
    "hello",
    "to",
    "API"
  ],
  "languageCode": "en-US",
  "transcription": "hello everyone this is going to be a short message where I'm going to repeat the same words over and over again to check if the API is really working if they'd be a really works I will be able to plug this MP3 recording to the server and it's going to be recognized",
  "wordCount": 54,
  "wordsSearch": {
    "hello": [
      {
        "startTime": "0.300s",
        "endTime": "0.900s",
        "word": "hello"
      }
    ],
    "to": [
      {
        "startTime": "2s",
        "endTime": "2.100s",
        "word": "to"
      },
      {
        "startTime": "3.700s",
        "endTime": "3.900s",
        "word": "to"
      },
      {
        "startTime": "5.800s",
        "endTime": "6.200s",
        "word": "to"
      },
      {
        "startTime": "10.800s",
        "endTime": "11s",
        "word": "to"
      },
      {
        "startTime": "12.500s",
        "endTime": "12.600s",
        "word": "to"
      },
      {
        "startTime": "14s",
        "endTime": "14.100s",
        "word": "to"
      }
    ],
    "API": [
      {
        "startTime": "6.800s",
        "endTime": "7.300s",
        "word": "API"
      }
    ]
  },
  "recognitionApiResults": {
    "results": [
      {
        "alternatives": [
          {
            "transcript": "hello everyone this is going to be a short message where I'm going to repeat the same words over and over again to check if the API is really working if they'd be a really works I will be able to plug this MP3 recording to the server and it's going to be recognized",
            "confidence": 0.69701469,
            "words": [
              {
                "startTime": "0.300s",
                "endTime": "0.900s",
                "word": "hello"
              },
              {
                "startTime": "0.900s",
                "endTime": "1.300s",
                "word": "everyone"
              },
              {
                "startTime": "1.300s",
                "endTime": "1.600s",
                "word": "this"
              },
              {
                "startTime": "1.600s",
                "endTime": "1.800s",
                "word": "is"
              },
              {
                "startTime": "1.800s",
                "endTime": "2s",
                "word": "going"
              },
              {
                "startTime": "2s",
                "endTime": "2.100s",
                "word": "to"
              },
              {
                "startTime": "2.100s",
                "endTime": "2.300s",
                "word": "be"
              },
              {
                "startTime": "2.300s",
                "endTime": "2.400s",
                "word": "a"
              },
              {
                "startTime": "2.400s",
                "endTime": "2.700s",
                "word": "short"
              },
              {
                "startTime": "2.700s",
                "endTime": "3s",
                "word": "message"
              },
              {
                "startTime": "3s",
                "endTime": "3.300s",
                "word": "where"
              },
              {
                "startTime": "3.300s",
                "endTime": "3.500s",
                "word": "I'm"
              },
              {
                "startTime": "3.500s",
                "endTime": "3.700s",
                "word": "going"
              },
              {
                "startTime": "3.700s",
                "endTime": "3.900s",
                "word": "to"
              },
              {
                "startTime": "3.900s",
                "endTime": "4.200s",
                "word": "repeat"
              },
              {
                "startTime": "4.200s",
                "endTime": "4.300s",
                "word": "the"
              },
              {
                "startTime": "4.300s",
                "endTime": "4.500s",
                "word": "same"
              },
              {
                "startTime": "4.500s",
                "endTime": "5s",
                "word": "words"
              },
              {
                "startTime": "5s",
                "endTime": "5.100s",
                "word": "over"
              },
              {
                "startTime": "5.100s",
                "endTime": "5.500s",
                "word": "and"
              },
              {
                "startTime": "5.500s",
                "endTime": "5.600s",
                "word": "over"
              },
              {
                "startTime": "5.600s",
                "endTime": "5.800s",
                "word": "again"
              },
              {
                "startTime": "5.800s",
                "endTime": "6.200s",
                "word": "to"
              },
              {
                "startTime": "6.200s",
                "endTime": "6.400s",
                "word": "check"
              },
              {
                "startTime": "6.400s",
                "endTime": "6.700s",
                "word": "if"
              },
              {
                "startTime": "6.700s",
                "endTime": "6.800s",
                "word": "the"
              },
              {
                "startTime": "6.800s",
                "endTime": "7.300s",
                "word": "API"
              },
              {
                "startTime": "7.300s",
                "endTime": "7.500s",
                "word": "is"
              },
              {
                "startTime": "7.500s",
                "endTime": "7.900s",
                "word": "really"
              },
              {
                "startTime": "7.900s",
                "endTime": "8.300s",
                "word": "working"
              },
              {
                "startTime": "8.300s",
                "endTime": "8.700s",
                "word": "if"
              },
              {
                "startTime": "8.700s",
                "endTime": "8.800s",
                "word": "they'd"
              },
              {
                "startTime": "8.800s",
                "endTime": "9s",
                "word": "be"
              },
              {
                "startTime": "9s",
                "endTime": "9.200s",
                "word": "a"
              },
              {
                "startTime": "9.200s",
                "endTime": "9.700s",
                "word": "really"
              },
              {
                "startTime": "9.700s",
                "endTime": "10.100s",
                "word": "works"
              },
              {
                "startTime": "10.100s",
                "endTime": "10.200s",
                "word": "I"
              },
              {
                "startTime": "10.200s",
                "endTime": "10.400s",
                "word": "will"
              },
              {
                "startTime": "10.400s",
                "endTime": "10.600s",
                "word": "be"
              },
              {
                "startTime": "10.600s",
                "endTime": "10.800s",
                "word": "able"
              },
              {
                "startTime": "10.800s",
                "endTime": "11s",
                "word": "to"
              },
              {
                "startTime": "11s",
                "endTime": "11.200s",
                "word": "plug"
              },
              {
                "startTime": "11.200s",
                "endTime": "11.400s",
                "word": "this"
              },
              {
                "startTime": "11.400s",
                "endTime": "11.800s",
                "word": "MP3"
              },
              {
                "startTime": "11.800s",
                "endTime": "12.500s",
                "word": "recording"
              },
              {
                "startTime": "12.500s",
                "endTime": "12.600s",
                "word": "to"
              },
              {
                "startTime": "12.600s",
                "endTime": "12.700s",
                "word": "the"
              },
              {
                "startTime": "12.700s",
                "endTime": "13.100s",
                "word": "server"
              },
              {
                "startTime": "13.100s",
                "endTime": "13.600s",
                "word": "and"
              },
              {
                "startTime": "13.600s",
                "endTime": "13.700s",
                "word": "it's"
              },
              {
                "startTime": "13.700s",
                "endTime": "14s",
                "word": "going"
              },
              {
                "startTime": "14s",
                "endTime": "14.100s",
                "word": "to"
              },
              {
                "startTime": "14.100s",
                "endTime": "14.300s",
                "word": "be"
              },
              {
                "startTime": "14.300s",
                "endTime": "15.100s",
                "word": "recognized"
              }
            ]
          }
        ],
        "resultEndTime": "15.300s",
        "languageCode": "en-us"
      }
    ],
    "totalBilledTime": "30s"
  },
  "sentimentAnalysisScore": -0.4,
  "sentimentAnalysisMagnitude": 0.4,
  "sentimentAnalysisDescription": "Negative",
  "sentimentAnalysisApiResult": {
    "magnitude": 0.4,
    "score": -0.4,
    "description": "Negative"
  },
  "createdAt": "2022-05-15T19:02:23+00:00"
}
```
