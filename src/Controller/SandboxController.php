<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\File;
use App\Service\TranscriptionHelper;

use Google\Cloud\Language\LanguageClient;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SandboxController extends AbstractController
{
    #[Route('/sandbox/transcription', name: 'app_sandbox_transcription')]
    public function transcription(TranscriptionHelper $transcriptionHelper): Response
    {
        $file = new File($this->getParameter("uploads_directory") . '/demo.mp3');
        
        $transcription = $transcriptionHelper->transcriptLocalAudioFile($file, "en-US");
        
        $transcriptionJson = $transcription->serializeToJsonString();
        
        dump($transcription);
        
        dump($transcriptionJson);

        return $this->render('sandbox/index.html.twig', [
            'controller_name' => 'SandboxController',
        ]);
    }
    
    #[Route('/sandbox/sentiment', name: 'app_sandbox_sentiment')]
    public function sentiment(TranscriptionHelper $transcriptionHelper, ParameterBagInterface $parameterBag): Response
    {
        $text = "It's not so good, but neither that bad";
        
        $language = new LanguageClient([
            "keyFile" => json_decode(file_get_contents($parameterBag->get("google_cloud_credentials_file")), true)
        ]);
        
        /* @var $annotation \Google\Cloud\Language\Annotation */
        $annotation = $language->analyzeSentiment($text);
        
        $result = $annotation->sentiment();
        
        $score = (float) $result["score"];
            
        switch($score){
            case ($score >= 0.8):
                $sentiment = "Very positive";
                break;
            case ($score <= -0.6):
                $sentiment = "Very negative";
                break;
            case ($score >= -0.6 && $score <= -0.1):    
                $sentiment = "Negative";
                break;
            case ($score >= 0.1 && $score <= 0.7):    
                $sentiment = "Positive";
                break;
            case ($score == 0):    
                $sentiment = "Neutral";
                break;
        }
        
        $result["text"] = $sentiment;
        
        if ($annotation->sentiment() > 0) {
            echo "This is a {$sentiment} message.\n";
        }
        
        dump($result);

        return $this->render('sandbox/index.html.twig', [
            'controller_name' => 'SandboxController',
        ]);
    }
}
