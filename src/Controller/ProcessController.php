<?php

namespace App\Controller;
 
use App\Entity\Process;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use App\Service\FileUploader;
use App\Service\TranscriptionHelper;
use App\Service\SentimentAnalysisHelper;
 
#[AsController]
final class ProcessController extends AbstractController
{
    public function __invoke(
        Request $request,
        FileUploader $fileUploader,
        TranscriptionHelper $transcriptionHelper,
        SentimentAnalysisHelper $sentimentAnalysis,
        ParameterBagInterface $parameterBag
    ): Process
    {
        /* @var $uploadedFile UploadedFile */
        $uploadedFile = $request->files->get('file');
        $langCode = $request->request->get("languageCode");
        
        $inputWords = json_decode($request->get("words")) ?? [];

        if (!$uploadedFile) {
            throw new BadRequestHttpException('"Audio File" is required');
        }
        
        $uploadedFilename = $fileUploader->upload($uploadedFile);
        
        // 1. Execute the transcription from SpeechToText
        $transcription = $transcriptionHelper->transcriptLocalAudioFile(
            new File($parameterBag->get("uploads_directory") . "/$uploadedFilename"),
            $langCode
        );
        
        $transcriptionResults = json_decode($transcription->serializeToJsonString(), true);
        
        // A string with the transcription of the audio file 
        $text = $transcriptionResults["results"][0]["alternatives"][0]["transcript"];
        
        // An array with the words identified in the audio file and the timestamp when they were spoken
        $words = $transcriptionResults["results"][0]["alternatives"][0]["words"];
        
        // Execute Sentiment Analysis
        $analysis = $sentimentAnalysis->analyze($text);
        
        $wordsSearch = [];
        
        foreach($words as $wordItem){
            if(in_array($wordItem["word"], $inputWords)){
                $wordsSearch[$wordItem["word"]][] = $wordItem;
            }
        }
        
        // 3. Build entity
        $process = new Process();
        $process
            ->setWords($inputWords)
            ->setAudioFile($uploadedFilename)
            ->setLanguageCode($langCode)
            ->setTranscription($text)
            ->setWordCount(count($words))
            ->setWordsSearch($wordsSearch)
            ->setSentimentAnalysisMagnitude($analysis["magnitude"])
            ->setSentimentAnalysisScore($analysis["score"])
            ->setSentimentAnalysisDescription($analysis["description"])
            ->setSentimentAnalysisApiResult($analysis)
            ->setRecognitionApiResults($transcriptionResults)
        ;

        return $process;
    }
}