<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\File;

use Google\Cloud\Speech\V1p1beta1\RecognitionConfig\AudioEncoding;
use Google\Cloud\Speech\V1p1beta1\RecognitionConfig;
use Google\Cloud\Speech\V1p1beta1\StreamingRecognitionConfig;
use Google\Cloud\Speech\V1p1beta1\SpeechClient;
use Google\Cloud\Speech\V1p1beta1\RecognitionAudio;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Helper class to transcript a local audio file using the Google Cloud Speech-To-Text API.
 * 
 * @author Carlos Delgado <dev@ourcodeworld.com>
 */
class TranscriptionHelper 
{
    private SpeechClient $speechClient;
    
    public function __construct(ParameterBagInterface $parameterBag) 
    {
        $this->speechClient = new SpeechClient([
            'credentials' => $parameterBag->get("google_cloud_credentials_file")
        ]);
    }
    
    /**
     * This method allows to transcript (convert Speech to Text) short audio files (up to 60 seconds) using
     * the Google Cloud SpeechToText API.
     * 
     * @param File $file
     * @param string $languageCode
     * @return string
     * @throws Exception
     */
    public function transcriptLocalAudioFile(File $file, string $languageCode = 'en-US')
    {
        $recognitionConfig = new RecognitionConfig();
        $recognitionConfig->setEnableWordTimeOffsets(true);
        // I'll use the V1p1beta1 of SpeechToText to provide support for MP3 directly.
        // If there were more time to present the test, a conversor from MP3 to WAV would be ideal using FFMPEG.
        switch($file->getExtension()){
            case "wav":
                $recognitionConfig
                    ->setEncoding(AudioEncoding::LINEAR16)
                    ->setSampleRateHertz(44100)
                    ->setLanguageCode($languageCode)
                    ->setAudioChannelCount(2);
                break;
            case "mp3":
                $recognitionConfig
                    ->setEncoding(AudioEncoding::MP3)
                    ->setSampleRateHertz(8000)
                    ->setLanguageCode($languageCode);
                break;
        }
        
        $config = new StreamingRecognitionConfig();
        $config->setConfig($recognitionConfig);
        
        $recognitionAudio = new RecognitionAudio([
            "content" => $file->getContent()
        ]);
        
        $data = $this->speechClient->recognize($recognitionConfig, $recognitionAudio);
            
        return $data;
    }
}