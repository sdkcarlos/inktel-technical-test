<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Google\Cloud\Language\LanguageClient;

/**
 * Helper class to analyze a piece of text/document to determine the sentiment.
 * 
 * @author Carlos Delgado <dev@ourcodeworld.com>
 */
class SentimentAnalysisHelper
{
    private LanguageClient $languageClient;
    
    public function __construct(ParameterBagInterface $parameterBag) 
    {
        $this->languageClient = new LanguageClient([
            "keyFile" => json_decode(
                file_get_contents($parameterBag->get("google_cloud_credentials_file")),
                true
            )
        ]);
    }
    
    public function analyze(string $text)
    {
        /* @var $annotation \Google\Cloud\Language\Annotation */
        $annotation = $this->languageClient->analyzeSentiment($text);
        
        $result = $annotation->sentiment();
        
        $score = (float) $result["score"];
            
        switch($score){
            case ($score >= 0.8):
                $sentiment = "Very positive";
                break;
            case ($score <= -0.6):
                $sentiment = "Very negative";
                break;
            case ($score >= -0.6 && $score <= -0.1):    
                $sentiment = "Negative";
                break;
            case ($score >= 0.1 && $score <= 0.7):    
                $sentiment = "Positive";
                break;
            case ($score == 0):    
                $sentiment = "Neutral";
                break;
        }
        
        $result["description"] = $sentiment;
        
        return $result;
    }
}