<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Process
 *
 * @ORM\Table(name="process")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}},
 *     collectionOperations={
 *         "post"={
 *             "controller"=App\Controller\ProcessController::class,
 *             "deserialize"=false,
 *             "openapi_context"={
 *                 "parameters"={
 *                      {
 *                          "name"="file",
 *                          "description"="MP3 or WAV file containing the spoken text to be transcribed.",
 *                          "in"="path",
 *                      },
 *                      {
 *                          "name"="words",
 *                          "description"="An array of words with which the system should look for to create an output of the number of times this word was used.",
 *                          "in"="path",
 *                      },
 *                      {
 *                          "name"="languageCode",
 *                          "description"="ISO 639-1 Code of the language in the audio file e.g. en-US, es-ES",
 *                          "in"="path",
 *                      },
 *                 },
 *                 "requestBody"={
 *                     "content"={
 *                         "multipart/form-data"={
 *                             "schema"={
 *                                 "type"="object",
 *                                 "properties"={
 *                                     "file"={
 *                                         "type"="string",
 *                                         "format"="binary"
 *                                     },
 *                                     "words"={
 *                                         "type"="object",
 *                                         "example"={"word1", "word2"}
 *                                     },
 *                                     "languageCode"={
 *                                         "type"="string",
 *                                         "example"="en-US"
 *                                     },
 *                                 }
 *                             }
 *                         }
 *                     }
 *                 }
 *             }
 *         },
 *         "get",
 *     },
 * )
 */
class Process
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="audiofile", type="text", length=65535, nullable=false)
     * @Groups({"read", "write"})
     */
    private $audiofile;

    /**
     * @var array|null
     *
     * @ORM\Column(name="words", type="json", nullable=true)
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="array",
     *             "example"={"word1", "word2"}
     *         }
     *     }
     * )
     * @Groups({"read", "write"})
     */
    private $words;
    
    /**
     * @var string
     *
     * @ORM\Column(name="language_code", type="text", length=65535, nullable=false)
     * @Groups({"read", "write"})
     */
    private $languageCode;
    
    /**
     * @var string|null
     *
     * @ORM\Column(name="transcription", type="text", nullable=true)
     * @Groups({"read"})
     */
    private $transcription;
    
    /**
     * @var integer|null
     *
     * @ORM\Column(name="word_count", type="integer", nullable=true)
     * @Groups({"read"})
     */
    private $wordCount;
    
    /**
     * @var array|null
     *
     * @ORM\Column(name="word_search", type="json", nullable=true)
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="array"
     *         }
     *     }
     * )
     * @Groups({"read", "write"})
     */
    private $wordsSearch;

    /**
     * @var string|null
     *
     * @ORM\Column(name="recognition_api_results", type="json", nullable=true)
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="array"
     *         }
     *     }
     * )
     * @Groups({"read", "write"})
     */
    private $recognitionApiResults;
    
    /**
     * @var float|null
     *
     * @ORM\Column(name="sentiment_analysis_score", type="float", nullable=true)
     * @Groups({"read"})
     */
    private $sentimentAnalysisScore;
    
    /**
     * @var float|null
     *
     * @ORM\Column(name="sentiment_analysis_magnitude", type="float", nullable=true)
     * @Groups({"read"})
     */
    private $sentimentAnalysisMagnitude;
    
    /**
     * @var string|null
     *
     * @ORM\Column(name="sentiment_analysis_description", type="text", length=65535, nullable=false)
     * @Groups({"read"})
     */
    private $sentimentAnalysisDescription;
    
    /**
     * @var string|null
     *
     * @ORM\Column(name="sentiment_analysis_api_result", type="json", nullable=false)
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="array"
     *         }
     *     }
     * )
     * @Groups({"read"})
     */
    private $sentimentAnalysisApiResult;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Groups("read")
     */
    private $createdAt;

    /**
     * Prepersist gets triggered on Insert
     * @ORM\PrePersist
     */
    public function updatedTimestamps()
    {
        if ($this->createdAt == null) {
            $this->createdAt = new \DateTime('now');
        }
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAudiofile(): ?string
    {
        return $this->audiofile;
    }

    public function setAudiofile(string $audiofile): self
    {
        $this->audiofile = $audiofile;

        return $this;
    }

    public function getWords()
    {
        return $this->words;
    }

    public function setWords($words): self
    {
        $this->words = $words;

        return $this;
    }
    
    public function getLanguageCode()
    {
        return $this->languageCode;
    }
    
    public function setLanguageCode(string $languageCode): self
    {
        $this->languageCode = $languageCode;
        
        return $this;
    }
    
    public function getTranscription()
    {
        return $this->transcription;
    }
    
    public function setTranscription(string $transcription)
    {
        $this->transcription = $transcription;
        
        return $this;
    }

    public function getRecognitionApiResults()
    {
        return $this->recognitionApiResults;
    }

    public function setRecognitionApiResults($recognitionApiResults): self
    {
        $this->recognitionApiResults = $recognitionApiResults;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getSentimentAnalysisScore(): ?float 
    {
        return $this->sentimentAnalysisScore;
    }

    public function getSentimentAnalysisMagnitude(): ?float 
    {
        return $this->sentimentAnalysisMagnitude;
    }

    public function getSentimentAnalysisDescription(): ?string 
    {
        return $this->sentimentAnalysisDescription;
    }

    public function getSentimentAnalysisApiResult()
    {
        return $this->sentimentAnalysisApiResult;
    }

    public function setSentimentAnalysisScore(?float $sentimentAnalysisScore)
    {
        $this->sentimentAnalysisScore = $sentimentAnalysisScore;
        
        return $this;
    }

    public function setSentimentAnalysisMagnitude(?float $sentimentAnalysisMagnitude)
    {
        $this->sentimentAnalysisMagnitude = $sentimentAnalysisMagnitude;
        
        return $this;
    }

    public function setSentimentAnalysisDescription(?string $sentimentAnalysisDescription)
    {
        $this->sentimentAnalysisDescription = $sentimentAnalysisDescription;
        
        return $this;
    }

    public function setSentimentAnalysisApiResult($sentimentAnalysisApiResult)
    {
        $this->sentimentAnalysisApiResult = $sentimentAnalysisApiResult;
        
        return $this;
    }
    
    public function getWordCount(): ?int 
    {
        return $this->wordCount;
    }

    public function getWordsSearch(): ?array 
    {
        return $this->wordsSearch;
    }

    public function setWordCount(?int $wordCount) 
    {
        $this->wordCount = $wordCount;
        
        return $this;
    }

    public function setWordsSearch(?array $wordsSearch) 
    {
        $this->wordsSearch = $wordsSearch;
        
        return $this;
    }
}
